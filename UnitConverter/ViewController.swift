/*

This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike
4.0 International License, by Yong Bakos.

*/

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate {
    
    let  userDefaultsLastRowKey = "defaultCelsiusPickerRow"

    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var celsuisPicker: UIPickerView!
    
    @IBOutlet var temperatureRange: TemperatureRange!
    
    private let converter = UnitConverter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let defaultPickerRow = celsuisPicker.numberOfRowsInComponent(0)/2
        celsuisPicker.selectRow(defaultPickerRow, inComponent: 0, animated: false)
        pickerView(celsuisPicker, didSelectRow: defaultPickerRow, inComponent: 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func initialPickerRow() ->Int{
        let savedRow = NSUserDefaults.standardUserDefaults().objectForKey(userDefaultsLastRowKey) as? Int
        
        if let row = savedRow {
            return row
        }
        else{
            return celsuisPicker.numberOfRowsInComponent(0)/2
        }
    }

    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let celsiusValue = temperatureRange.values[row]
        return "\(celsiusValue)°C"
    }
    
    func displayConvertedTemperatureForRow(row: Int) {
        let degreesCelsius = temperatureRange.values[row]
        temperatureLabel.text = "\(Int(converter.degreesFahrenheit(degreesCelsius)))°F"
    }
    

    func saveSelectedRow(row: Int){
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setInteger(row, forKey: "defaultCelsiusPickerRow")
        defaults.synchronize()
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        displayConvertedTemperatureForRow(row)
        saveSelectedRow(row)
    }

}
